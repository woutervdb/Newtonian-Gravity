import {Renderer} from './renderer'
import {Simulation} from '../simulation/simulation'
import {Particle} from '../simulation/particle'

export class RendererParticles extends Renderer {
  public static momemtumScale = 0.1

  public render() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

    for (let particle of this.simulation.particles) {
      if (particle.position.x < 0 || particle.position.x > 1
        || particle.position.y < 0 || particle.position.y > 1) continue
      this.renderParticle(particle)
    }
  }

  private renderParticle(particle: Particle) {
    this.ctx.beginPath()

    // Position
    this.ctx.fillStyle = 'white'
    this.ctx.arc(particle.position.x * this.canvas.width - particle.mass * 2.5, particle.position.y * this.canvas.height - particle.mass * 2.5, particle.mass * 5, 0, Math.PI * 2)
    this.ctx.fill()

    // Momemtum
    // this.ctx.strokeStyle = 'white'
    // this.ctx.lineWidth = 1
    // this.ctx.moveTo(px * this.canvas.width, py * this.canvas.height)
    // this.ctx.lineTo((px + particle.momemtum.x * RendererParticles.momemtumScale) * this.canvas.width, (py + particle.momemtum.y * RendererParticles.momemtumScale) * this.canvas.height)
    // this.ctx.stroke()

    this.ctx.closePath()
  }
}