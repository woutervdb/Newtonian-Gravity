import {Simulation} from '../simulation/simulation'

export abstract class Renderer {
  protected ctx: CanvasRenderingContext2D

  constructor(protected simulation: Simulation, protected canvas: HTMLCanvasElement) {
    this.ctx = canvas.getContext('2d')
  }

  public abstract render()
}
