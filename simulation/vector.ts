export class Vector {
  public static zero = new Vector(0,0)

  constructor(private _x: number, private _y: number) {}

  private _magSq: number
  private _mag: number

  public get x(): number { return this._x }
  public get y(): number { return this._y }
  public get magnitudeSq(): number { 
    if (this._magSq == null) {
      return this._magSq = this.x * this.x + this.y * this.y
    } else {
      return this._magSq
    }
  }
  public get magnitude(): number {
    if (this._mag == null) {
      return this._mag = Math.sqrt(this.magnitudeSq)
    } else {
      return this._mag
    }
  }

  public distanceSqTo(x: number, y: number) {
    let dx = this.x - x
    let dy = this.y - y

    return dx * dx + dy * dy
  }

  public distanceTo(x: number, y: number) {
    return Math.sqrt(this.distanceSqTo(x, y))
  }

  public add(other: Vector)
  public add(x: number, y: number)
  public add() {
    let x: number, y: number
    if (arguments.length === 1) {
      x = arguments[0].x, y = arguments[0].y
    } else {
      [x,y] = arguments
    }
    return new Vector(this.x + x, this.y + y)
  }

  public subtract(other: Vector)
  public subtract(x: number, y: number)
  public subtract() {
    let x: number, y: number
    if (arguments.length === 1) {
      let {x,y} = arguments[0]
    } else {
      [x,y] = arguments
    }
    return new Vector(this.x - x, this.y - y)
  }

  public times(s: number) {
    return new Vector(this.x * s, this.y * s)
  }

  public withMagnitude(mag: number) {
    if (this.magnitudeSq < 3e-5 || Number.isNaN(this.x * mag / this.magnitude) || Number.isNaN(this.y * mag / this.magnitude)) {
      return Vector.zero
    }
    return new Vector(this.x * mag / this.magnitude, this.y * mag / this.magnitude)
  }
}