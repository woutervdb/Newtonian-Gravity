import {Vector} from './vector'
import {Particle} from './particle'

export class Simulation {
  private _particles: Particle[]

  public get particles() { return this._particles }

  constructor(private G: number, private maxMass: number, private dt: number) {
    this._particles = []
  }

  public generateParticles(count: number, generatorPosition: () => number = Math.random, generatorMomemtum: () => number = () => 0, generatorMass: () => number = Math.random) {
    for (let i = 0; i < count; i++) {
      let position = new Vector(generatorPosition(), generatorPosition())
      let momemtum = new Vector(generatorMomemtum(), generatorMomemtum())
      let mass = generatorMass() * this.maxMass

      let particle = new Particle(position, momemtum, mass)

      this._particles.push(particle)
    }
  }

  public getForceAt(x: number, y: number, mass: number) {
    let totalForce = Vector.zero

    for (let particle of this._particles) {
      let force = this.G * particle.mass / particle.distanceSqTo(x, y)

      totalForce = totalForce.add(particle.position.subtract(x,y).withMagnitude(force))
    }

    return totalForce.times(mass)
  }

  public update() {
    let newParticles = []

    for (let particle of this._particles) {
      newParticles.push(this.calculateNewParticle(particle))
    }

    this._particles = newParticles
  }

  private calculateNewParticle(particle: Particle) {
    // F = GMm/r^2
    let force = this.getForceAt(particle.position.x, particle.position.y, particle.mass)

    // p = F dt
    let momemtum = particle.momemtum.add(force.times(this.dt))
    // x = p/m dt
    let position = particle.position.add(momemtum.times(this.dt / particle.mass))

    return new Particle(position, momemtum, particle.mass)
  }
}