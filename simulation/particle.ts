import {Vector} from './vector'

export class Particle {
  public get position() { return this._position }
  public get momemtum() { return this._momemtum }
  public get mass() { return this._mass }

  constructor(private _position: Vector, private _momemtum: Vector, private _mass: number) {}

  public distanceSqTo(x: number, y: number) {
    return this.position.distanceSqTo(x,y)
  }

  public distanceTo(x: number, y: number) {
    return this.position.distanceTo(x,y)
  }
}