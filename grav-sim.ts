import {Simulation} from './simulation/simulation'
import {Renderer} from './render/renderer'
import {RendererParticles} from './render/renderer-particles'
import {Recorder} from './recorder'

let shouldRecord = false,
  maxRecordedFrames = 3000,
  frameRenderHandle
let lastFrameFinished = true,
  msPerFrame = 1000/60,
  canvas: HTMLCanvasElement
let valueMap

let G = 5e-5,
  maxMass = 1,
  dt = 1/300,
  particleCount = 100

let simulation: Simulation,
  renderer: Renderer,
  recorder: Recorder

function onLoaded() {
  canvas = document.getElementById('canvas') as HTMLCanvasElement

  simulation = new Simulation(G, maxMass, dt)
  renderer = new RendererParticles(simulation, canvas)
  recorder = new Recorder(canvas.getContext('2d'))

  window.addEventListener('keydown', onKeyDown)
  window.addEventListener('resize', onResizeCanvas)

  onResizeCanvas()
  startSimulation()
}

function onKeyDown(e: KeyboardEvent) {
  if (e.which != null) {
    if (e.which === 27 && frameRenderHandle != null) {
      stopSimulation()
      e.preventDefault()
    } else if (e.which === 13 && frameRenderHandle == null) {
      continueSimulation()
    }
  }
}

function onResizeCanvas() {
  canvas.width = document.body.clientWidth
  canvas.height = document.body.clientHeight
}

function loop() {
  if (shouldRecord && recorder.frameCount >= maxRecordedFrames) {
    stopSimulation()
  } else if (lastFrameFinished) {
    lastFrameFinished = false

    simulation.update()
    renderer.render()
    if (shouldRecord) recorder.recordFrame()

    lastFrameFinished = true
  } else {
    console.count('frame-miss')
  }
}

function startSimulation() {
  simulation.generateParticles(particleCount)
  renderer.render()
  if (shouldRecord) recorder.recordFrame()

  continueSimulation()
}

function continueSimulation() {
  frameRenderHandle = setInterval(loop, msPerFrame)
}

function stopSimulation() {
  clearInterval(frameRenderHandle)
  recorder.startPlayback(msPerFrame)
  frameRenderHandle = null
}

function parseHash() {
  let hash = window.location.hash.substr(1)

  valueMap = {}
  let configPairs = hash.split('&')

  for (let currentPair of configPairs) {
    let [k,v] = currentPair.split('=', 2)

    if (v == null) {
      valueMap[k] = true
    }
    valueMap[k] = v
  }
}

function updateVariables() {
  shouldRecord =  valueMap['record'] === "true" || shouldRecord
  maxRecordedFrames = Number.parseInt(valueMap['maxFrames']) || maxRecordedFrames
  msPerFrame = 1000 / Number.parseInt(valueMap['fps']) || msPerFrame
  G = Number.parseFloat(valueMap['G']) || G
  maxMass = Number.parseFloat(valueMap['m']) || maxMass
  dt = Number.parseFloat(valueMap['dt']) || dt
  particleCount = Number.parseInt(valueMap['n']) || particleCount
}

parseHash()
updateVariables()

if (document.readyState === 'complete') {
  onLoaded()
} else {
  window.addEventListener('load', onLoaded)
}