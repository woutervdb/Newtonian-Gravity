export class Recorder {
  private frames: ImageData[]
  private playbackId: number

  public get frameCount() { return this.frames.length }

  constructor(private ctx: CanvasRenderingContext2D) {
    this.frames = []
  }

  public recordFrame() {
    let frame = this.ctx.getImageData(0, 0, this.ctx.canvas.width, this.ctx.canvas.height)
    this.frames.push(frame)
  }

  public startPlayback(interval: number) {
    if (this.playbackId != null) {
      clearInterval(this.playbackId)
    }

    let currentFrameNumber = 0
    this.playbackId = setInterval(() => {
      currentFrameNumber = (currentFrameNumber + 1) % this.frames.length
      this.showFrame(currentFrameNumber)
    }, interval)
  }

  public stopPlayback() {
    clearInterval(this.playbackId)
    this.playbackId = null
  }

  private showFrame(frameNumber: number) {
    let currentFrame = this.frames[frameNumber]

    this.ctx.putImageData(currentFrame, 0, 0)
  }
}